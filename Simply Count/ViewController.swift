//
//  ViewController.swift
//  Simple Count
//
//  Created by Zach Eriksen on 1/7/16.
//  Copyright © 2016 Zach Eriksen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let defaults = NSUserDefaults.standardUserDefaults()
    var label : UILabel!
    var plus : UIButton!
    var minus : UIButton!
    var clear : UIButton!
    let font = UIFont(name: "Kailasa", size: 50)
    var currentYLocation : CGFloat = 40
    var number = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        load()
        createLabel()
        createAddButton()
        createSubButton()
        createClearButton()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func createRectWithCurrentYLocation(height : CGFloat) -> CGRect{
        let rect = CGRectMake(0, currentYLocation, screenWidth, height)
        currentYLocation += height
        return rect
    }
    
    private func createLabel(){
        label = UILabel(frame: CGRectMake(0,0,screenWidth,screenHeight))
        label.backgroundColor = UIColor.rgba(255,158,128 ,1)
        label.font = UIFont(name: "Kailasa", size: 500)
        label.textColor = .whiteColor()
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.1
        label.textAlignment = .Center
        label.text = "\(number)"
        view.addSubview(label)
    }
    
    private func createAddButton(){
        plus = UIButton(frame: CGRectMake((screenWidth*2)/3,(screenHeight*4)/5,screenWidth/3,screenHeight/5))
        plus.setTitleColor(.whiteColor(), forState: .Normal)
        plus.titleLabel?.font = font
        plus.titleLabel?.textAlignment = .Center
        plus.setTitle("+", forState: .Normal)
        plus.addTarget(self, action: "add", forControlEvents: .TouchUpInside)
        view.addSubview(plus)
    }
    
    private func createSubButton(){
        minus = UIButton(frame: CGRectMake(0,(screenHeight*4)/5,screenWidth/3,screenHeight/5))
        minus.setTitleColor(.whiteColor(), forState: .Normal)
        minus.titleLabel?.font = font
        minus.titleLabel?.textAlignment = .Center
        minus.setTitle("-", forState: .Normal)
        minus.addTarget(self, action: "sub", forControlEvents: .TouchUpInside)
        view.addSubview(minus)
    }
    
    private func createClearButton(){
        clear = UIButton(frame: CGRectMake(screenWidth/3,(screenHeight*4)/5,screenWidth/3,screenHeight/5))
        clear.setTitleColor(.whiteColor(), forState: .Normal)
        clear.titleLabel?.font = font
        clear.titleLabel?.textAlignment = .Center
        clear.setTitle("C", forState: .Normal)
        clear.addTarget(self, action: "clearCount", forControlEvents: .TouchUpInside)
        view.addSubview(clear)
    }
    
    func load(){
        guard let count : Int = defaults.valueForKey("count") as? Int else {
            defaults.setValue(0, forKey: "count")
            return
        }
        number = count
        defaults.synchronize()
    }
    
    func save(){
        defaults.setValue(number, forKey: "count")
    }
    
    func clearCount(){
        number = 0
        label.text = "\(number)"
        save()
    }
    
    func sub(){
        number--
        label.text = "\(number)"
        save()
    }
    
    func add(){
        number++
        label.text = "\(number)"
        save()
    }
    
}

