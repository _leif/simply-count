//
//  Extension.swift
//
//  Created by Zach Eriksen on 1/4/16.
//  Copyright © 2016 Zach Eriksen. All rights reserved.
//

import Foundation
import UIKit

let screenHeight = UIScreen.mainScreen().bounds.height
let screenWidth = UIScreen.mainScreen().bounds.width

extension Int {
    func times(task: () -> ()) {
        for _ in 1...self {
            task()
        }
    }
    
    var isEven: Bool { return self % 2 == 0 }
    
    var isOdd: Bool { return !isEven }
    
    var isPositive: Bool { return self > 0 }
    
    var isNegative: Bool { return self < 0 }
    
    var toDouble: Double { return Double(self) }
    
    var toFloat: Float { return Float(self) }
    
    var digits: Int {
        if self == 0 {
            return 1
        }
        else if(Int(fabs(toDouble)) <= LONG_MAX) {
            return Int(log10(fabs(toDouble))) + 1
        }
        else {
            return -1;
        }
    }
    
    var isPrime: Bool {
        if self < 2  { return false }
        return (2..<self).filter { self % $0 == 0 }.count == 0
    }
}

extension Double {
    func round(decimals: Int) -> Double {
        let format : NSNumberFormatter = NSNumberFormatter()
        format.numberStyle = NSNumberFormatterStyle.DecimalStyle
        format.roundingMode = NSNumberFormatterRoundingMode.RoundHalfUp
        format.maximumFractionDigits = decimals
        let string: NSString = format.stringFromNumber(NSNumber(double: self))!
        return string.doubleValue
    }
}

extension String {
    var length : Int { return self.characters.count }
    subscript (i: Int) -> Character {
        return self[self.startIndex.advancedBy(i)]
    }
    
    func reverse() -> String {
        return (1...length)
            .map { "\(self[length - $0])" }
            .joinWithSeparator("")
    }
    
    func toCharArray() -> [Character] {
        var array : [Character] = []
        for var i = 0; i < self.length; i++ {
            array.append(self[i])
        }
        return array
    }
}

extension UIColor {
    static func randomColor() -> UIColor {
        func randomCGFloat() -> CGFloat {
            return CGFloat(arc4random()) / CGFloat(UInt32.max)
        }
        let r = randomCGFloat()
        let g = randomCGFloat()
        let b = randomCGFloat()
        return UIColor(red: r, green: g, blue: b, alpha: 1.0)
    }
    
    static func rgba(r : CGFloat, _ g : CGFloat, _ b :CGFloat, _ a : CGFloat) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: a)
    }
}
