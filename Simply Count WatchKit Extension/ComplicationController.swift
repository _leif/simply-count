//
//  ComplicationController.swift
//  Simple Count WatchKit Extension
//
//  Created by Zach Eriksen on 1/7/16.
//  Copyright © 2016 Zach Eriksen. All rights reserved.
//

import ClockKit


class ComplicationController: NSObject, CLKComplicationDataSource {
    
    // MARK: - Timeline Configuration
    
    func getSupportedTimeTravelDirectionsForComplication(complication: CLKComplication, withHandler handler: (CLKComplicationTimeTravelDirections) -> Void) {
        handler([.None])
    }
    
    func getTimelineStartDateForComplication(complication: CLKComplication, withHandler handler: (NSDate?) -> Void) {
        handler(nil)
    }
    
    func getTimelineEndDateForComplication(complication: CLKComplication, withHandler handler: (NSDate?) -> Void) {
        handler(nil)
    }
    
    func getPrivacyBehaviorForComplication(complication: CLKComplication, withHandler handler: (CLKComplicationPrivacyBehavior) -> Void) {
        handler(.ShowOnLockScreen)
    }
    
    // MARK: - Timeline Population
    
    func getCurrentTimelineEntryForComplication(complication: CLKComplication, withHandler handler: ((CLKComplicationTimelineEntry?) -> Void)) {
        // Call the handler with the current timeline entry
        let entry = CLKComplicationTimelineEntry()
        entry.date = NSDate()
        var number : Int = 0
        let defaults = NSUserDefaults.standardUserDefaults()
        guard let count : Int = defaults.valueForKey("count") as? Int else {
            print("Count 2 guard: 0")
            entry.complicationTemplate = makeComplicationTemplate("0", complication: complication)!
            handler(entry)
            return
        }
        number = count
        print("Count 2 else: \(number)")
        entry.complicationTemplate = makeComplicationTemplate("\(number)", complication: complication)!
        handler(entry)
    }
    
    func getTimelineEntriesForComplication(complication: CLKComplication, beforeDate date: NSDate, limit: Int, withHandler handler: (([CLKComplicationTimelineEntry]?) -> Void)) {
        // Call the handler with the timeline entries prior to the given date
        handler(nil)
    }
    
    func getTimelineEntriesForComplication(complication: CLKComplication, afterDate date: NSDate, limit: Int, withHandler handler: (([CLKComplicationTimelineEntry]?) -> Void)) {
        // Call the handler with the timeline entries after to the given date
        handler(nil)
    }
    
    // MARK: - Update Scheduling
    
    func getNextRequestedUpdateDateWithHandler(handler: (NSDate?) -> Void) {
        // Call the handler with the date when you would next like to be given the opportunity to update your complication content
        print("getNextDate")
        handler(NSDate(timeIntervalSinceNow: 60*60));
    }
    
    
    
    // MARK: - Placeholder Templates
    
    func getPlaceholderTemplateForComplication(complication: CLKComplication, withHandler handler: (CLKComplicationTemplate?) -> Void) {
        var number = "0"
        let defaults = NSUserDefaults.standardUserDefaults()
        if let count = defaults.valueForKey("count") as? String {
            number = count
        }
        print("count1: \(number)")
        handler(makeComplicationTemplate("\(number)", complication: complication))
    }
    
    private func makeComplicationTemplate(str : String, complication : CLKComplication) -> CLKComplicationTemplate? {
        switch complication.family {
        case .ModularSmall:
            let tmpl = CLKComplicationTemplateModularSmallSimpleText()
            tmpl.tintColor = UIColor.whiteColor()
            tmpl.textProvider = CLKSimpleTextProvider(text: "\(str)", shortText: "\(str)")
            tmpl.textProvider.tintColor = .whiteColor()
            return tmpl
        case .CircularSmall:
            let tmpl = CLKComplicationTemplateCircularSmallSimpleText()
            tmpl.tintColor = UIColor.whiteColor()
            tmpl.textProvider = CLKSimpleTextProvider(text: "\(str)", shortText: "\(str)")
            tmpl.textProvider.tintColor = .whiteColor()
            return tmpl
        case .UtilitarianSmall:
            let tmpl = CLKComplicationTemplateUtilitarianSmallFlat()
            tmpl.tintColor = UIColor.whiteColor()
            tmpl.textProvider = CLKSimpleTextProvider(text: "\(str)", shortText: "\(str)")
            tmpl.textProvider.tintColor = .whiteColor()
            return tmpl
        default:
            return nil
        }
    }
    
}
