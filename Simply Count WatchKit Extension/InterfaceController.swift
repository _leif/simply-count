//
//  InterfaceController.swift
//  Simple Count WatchKit Extension
//
//  Created by Zach Eriksen on 1/7/16.
//  Copyright © 2016 Zach Eriksen. All rights reserved.
//

import WatchKit
import Foundation
import ClockKit


class InterfaceController: WKInterfaceController {
    var number : Int = 0
    @IBOutlet var label: WKInterfaceLabel!
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        //use our group user defaults
        print("Awake")
    }
    
    @IBAction func plus() {
        number++
        label.setText("\(number)")
        save()
    }
    @IBAction func minus() {
        number--
        label.setText("\(number)")
        save()
    }
    @IBAction func clear() {
        number = 0
        label.setText("\(number)")
        save()
    }
    
    func load() {
        let defaults = NSUserDefaults.standardUserDefaults()//(suiteName: "group.com.leif.watch.count")
        
        guard let count : Int = defaults.valueForKey("count") as? Int else {
            defaults.setValue(0, forKey: "count")
            return
        }
        print("Count: \(count) Number: \(number)")
        number = count
        label.setText("\(number)")
        
        defaults.synchronize()
    }
    
    func save(){
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setValue(number, forKey: "count")
        let complicationServer = CLKComplicationServer.sharedInstance()
        
        for complication in complicationServer.activeComplications {
            complicationServer.reloadTimelineForComplication(complication)
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        load()
        print("willActivate!")
        super.willActivate()
    }
    
    override func pickerDidResignFocus(picker: WKInterfacePicker) {
        print("pickerDidResignRocus")
    }
    
    override func willDisappear() {
        print("willDisappear")
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        save()
        print("Deactivate")
        super.didDeactivate()
    }
    
}
